package org.uonse.components;

import org.uonse.app.Application;

public class LoadingSpinnerDemo extends Application {

	public static void main(String[] args) {
		NETWORKING = false;
		client.start();
		client.getContext().getSettings().setTitle("Loading Spinner Demo");
		client.setCurrentApp(new LoadingSpinnerDemo());
	}

	@Override
	public void registerComponents() {
		register(ILoadingSpinner.class, LoadingSpinner.class);
	}

	@Override
	public void onAppStartUp() {
		ILoadingSpinner spinner = create(ILoadingSpinner.class);
//		spinner.setRadius(1.0F);
//		spinner.setRingWidth(1.0F);
		spinner.setSize(700.0F);
//		spinner.setSpeed(0.01F);
		addItem(spinner);
	}

}
