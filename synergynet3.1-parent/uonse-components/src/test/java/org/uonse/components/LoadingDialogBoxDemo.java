package org.uonse.components;

import com.jme3.math.ColorRGBA;
import multiplicity3.csys.behaviours.RotateTranslateScaleBehaviour;
import org.uonse.app.Application;

public class LoadingDialogBoxDemo extends Application {

	public static void main(String[] args) {
		NETWORKING = false;
		client.start();
		client.getContext().getSettings().setTitle("Loading Dialog Box Demo");
		client.setCurrentApp(new LoadingDialogBoxDemo());
	}

	@Override
	public void registerComponents() {
		register(ILoadingDialogBox.class, LoadingDialogBox.class);
		register(ILoadingSpinner.class, LoadingSpinner.class);
	}

	@Override
	public void onAppStartUp() {
		setBackgroundColor(ColorRGBA.Gray);

		ILoadingDialogBox dialogBox = create(ILoadingDialogBox.class);
		RotateTranslateScaleBehaviour rts = addBehaviour(dialogBox, RotateTranslateScaleBehaviour.class);
		rts.setScaleLimits(-Float.MAX_VALUE, Float.MAX_VALUE);
		addItem(dialogBox);
	}

}
