package org.uonse.components;

import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import multiplicity3.csys.behaviours.RotateTranslateScaleBehaviour;
import org.uonse.app.Application;
import org.uonse.util.FasterMath;
import org.uonse.util.ColorUtils;

public class CircleDemo extends Application {

	public static void main(String[] args) {
		NETWORKING = false;
		client.start();
		client.setCurrentApp(new CircleDemo());
		setTitle("Circle Demo");
	}

	@Override
	public void registerComponents() {
		register(ICircle.class, Circle.class);
	}

	@Override
	public void onAppStartUp() {
		setBackgroundColor(ColorRGBA.White);
		addCircles();
	}

	private void addCircles() {
		int numCircles = 12;
		float circleSize = 100;
		float radius = 300;
		for (int i = 0; i < numCircles; i++) {
			ICircle circle = create(ICircle.class);
			addBehaviour(circle, RotateTranslateScaleBehaviour.class);

			circle.setSize(circleSize);
			circle.setRelativeLocation(FasterMath.fromPolar(radius, i * FastMath.TWO_PI / numCircles));
			circle.setColor(ColorUtils.HSV_to_RGBA(i / (float) numCircles, 1, 1));
			addItem(circle);
		}
	}

}
