package org.uonse.components;

import com.jme3.math.ColorRGBA;
import org.uonse.app.Application;

import java.util.Random;

public class GraphDemo extends Application {

	private static Random random = new Random();

	public static void main(String[] args) {
		NETWORKING = false;
		client.start();
		client.setCurrentApp(new GraphDemo());
		setTitle("Graph Demo");
	}

	@Override
	public void registerComponents() {
		register(ICircle.class, Circle.class);
		register(IGraphDisplay.class, GraphDisplay.class);
	}

	@Override
	public void onAppStartUp() {
		setBackgroundColor(ColorRGBA.White);
		IGraphDisplay graph = create(IGraphDisplay.class);
		addItem(graph);
	}

}

