#import "org/uonse/shaders/common.glsllib"

uniform float m_Time;
uniform float m_Radius;
uniform float m_RingWidth;
uniform float m_Period;

varying vec2 v_TexCoord;

void main () {

	// Base ring colour
	gl_FragColor = COLOR_BLUE;

	// Convert from uv coordinates [0,1] to [-1,1] with (0,0) as center.
	vec2 object_coord = 2.0 * (v_TexCoord - 0.5);

	// Calculate the outer and inner radii of the ring
	float min_distance = max(m_Radius - m_RingWidth, 0.0);
	float max_distance = m_Radius;

	// Calculate the distance that the current fragment is from the center of the ring
	float distance_from_center = length(object_coord);

	// Draw a ring by setting the alpha of anything outside the ring to 0
	gl_FragColor.a = smoothbump(min_distance, max_distance, distance_from_center);

	// Spin ring by applying a z-axis rotation matrix
	float angle = 2.0 * M_PI * m_Time / m_Period;
	vec2 rotated_coord = rot_z(angle) * object_coord;

	// Create alpha fade off, based on the angle around the ring
	float angle_unit = atan2_unit(rotated_coord);
	float ring_fade = 1.0 - angle_unit;

	// Anti-alias the leading edge
	float eps = 2.0 * fwidth(angle_unit);
	float edge_fade = min(smoothstep(0, eps, angle_unit), smoothstep(1.0, 1.0 - eps, angle_unit));

	// Finally combine fades and apply to alpha
	gl_FragColor.a *= min(ring_fade, edge_fade);

}
