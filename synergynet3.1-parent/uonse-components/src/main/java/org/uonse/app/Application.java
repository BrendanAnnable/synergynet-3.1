package org.uonse.app;

import com.jme3.asset.AssetManager;
import com.jme3.math.ColorRGBA;
import multiplicity3.appsystem.IQueueOwner;
import multiplicity3.appsystem.MultiplicityClient;
import multiplicity3.csys.behaviours.IBehaviour;
import multiplicity3.csys.factory.ContentTypeAlreadyBoundException;
import multiplicity3.csys.factory.ContentTypeInvalidException;
import multiplicity3.csys.factory.ContentTypeNotBoundException;
import multiplicity3.csys.items.item.IItem;
import multiplicity3.input.MultiTouchInputComponent;
import synergynet3.SynergyNetApp;

import java.util.UUID;

public abstract class Application extends SynergyNetApp implements ISimpleContentFactory {

	protected static final MultiplicityClient client = MultiplicityClient.get();
	protected static AssetManager assetManager = null;

	public <ContentType extends IItem, ConcreteType extends IItem> ConcreteType create(Class<ContentType> clazz) {

		UUID id = UUID.randomUUID();
		String name = "id-" + id;
		try {
			ConcreteType contentType = contentFactory.create(clazz, name, id);
			if (contentType instanceof IComponent) {
				IComponent component = (IComponent) contentType;
				component.setContentFactory(this);
				component.setBehaviourMaker(behaviourMaker);
				component.init();
			}
			return contentType;
		}
		catch (ContentTypeNotBoundException e) {
			e.printStackTrace();
			return null;
		}

	}

	public <ContentType extends IItem, ConcreteType extends IItem> void register(Class<ContentType> contentType, Class<ConcreteType> concreteType) {
		try {
			contentFactory.register(contentType, concreteType);
		}
		catch (ContentTypeAlreadyBoundException | ContentTypeInvalidException e) {
			e.printStackTrace();
		}
	}

	public <Behaviour extends IBehaviour> Behaviour addBehaviour(IItem item, Class<Behaviour> behaviourClass) {
		return behaviourMaker.addBehaviour(item, behaviourClass);
	}

	public static void setTitle(String title) {
		client.getContext().getSettings().setTitle(title);
	}

	public static void setBackgroundColor(ColorRGBA color) {
		client.getViewPort().setBackgroundColor(color);
	}

	public void addItem(IItem item) {
		stage.addItem(item);
	}

	@Override
	public void shouldStart(MultiTouchInputComponent input, IQueueOwner iqo) {

		super.shouldStart(input, iqo);
		assetManager = client.getAssetManager();
		registerComponents();
		onAppStartUp();

	}

	public void onAppStartUp() {

	}

	public void registerComponents() {

	}

}
