package org.uonse.app;

import multiplicity3.csys.items.item.IItem;

public interface ISimpleContentFactory {

	<ContentType extends IItem, ConcreteType extends IItem> ConcreteType create(Class<ContentType> clazz);
	<ContentType extends IItem, ConcreteType extends IItem> void register(Class<ContentType> contentType, Class<ConcreteType> concreteType);

}
