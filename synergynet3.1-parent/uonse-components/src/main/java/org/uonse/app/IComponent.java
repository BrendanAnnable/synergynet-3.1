package org.uonse.app;

import com.jme3.asset.AssetManager;
import multiplicity3.csys.behaviours.BehaviourMaker;
import multiplicity3.csys.items.item.IItem;

public interface IComponent extends IItem {

	void init();
	AssetManager getAssetManager();
	void setAssetManager(AssetManager assetManager);
	ISimpleContentFactory getContentFactory();
	void setContentFactory(ISimpleContentFactory contentFactory);
	BehaviourMaker getBehaviourMaker();
	void setBehaviourMaker(BehaviourMaker behaviourMaker);

}
