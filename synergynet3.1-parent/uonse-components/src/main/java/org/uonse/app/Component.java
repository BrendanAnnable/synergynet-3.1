package org.uonse.app;

import com.jme3.asset.AssetManager;
import multiplicity3.csys.behaviours.BehaviourMaker;
import multiplicity3.jme3csys.items.item.JMEItem;

import java.util.UUID;

public abstract class Component extends JMEItem implements IComponent {

	protected AssetManager assetManager = null;
	protected ISimpleContentFactory contentFactory = null;
	protected BehaviourMaker behaviourMaker = null;

	/**
	 * Instantiates a new JME item.
	 *
	 * @param name the name
	 * @param uuid the uuid
	 */
	public Component(String name, UUID uuid) {
		super(name, uuid);
	}

	public void initializeGeometry(AssetManager assetManager) {
		this.setAssetManager(assetManager);
	}

	public AssetManager getAssetManager() {
		return assetManager;
	}

	public void setAssetManager(AssetManager assetManager) {
		this.assetManager = assetManager;
	}

	public ISimpleContentFactory getContentFactory() {
		return contentFactory;
	}

	public void setContentFactory(ISimpleContentFactory contentFactory) {
		this.contentFactory = contentFactory;
	}

	public BehaviourMaker getBehaviourMaker() {
		return behaviourMaker;
	}

	public void setBehaviourMaker(BehaviourMaker behaviourMaker) {
		this.behaviourMaker = behaviourMaker;
	}
}
