package org.uonse.util;

import com.jme3.math.ColorRGBA;

public class ColorUtils {
	public static ColorRGBA HSV_to_RGBA(float hue, float saturation, float value) {

		// Adapted from http://stackoverflow.com/a/7898685/868679
		int h = (int)(hue * 6);
		float f = hue * 6 - h;
		float p = value * (1 - saturation);
		float q = value * (1 - f * saturation);
		float t = value * (1 - (1 - f) * saturation);

		switch (h) {
			case 0: return new ColorRGBA(value, t, p, 1.0F);
			case 1: return new ColorRGBA(q, value, p, 1.0F);
			case 2: return new ColorRGBA(p, value, t, 1.0F);
			case 3: return new ColorRGBA(p, q, value, 1.0F);
			case 4: return new ColorRGBA(t, p, value, 1.0F);
			case 5: return new ColorRGBA(value, p, q, 1.0F);
			default: throw new RuntimeException("Could not convert from HSV to RGB (" + hue + ", " + saturation + ", " + value + ")");
		}
	}
}
