package org.uonse.util;

import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;

public class FasterMath {
	public static Vector2f fromPolar(float distance, float angle) {
		return new Vector2f(distance * FastMath.cos(angle), distance * FastMath.sin(angle));
	}

	public static Vector3f fromSphere(float distance, float azimuthalAngle, float polarAngle) {
		return new Vector3f(
			distance * FastMath.sin(polarAngle) * FastMath.cos(azimuthalAngle),
			distance * FastMath.sin(polarAngle) * FastMath.sin(azimuthalAngle),
			distance * FastMath.cos(polarAngle)
		);
	}

	public static Vector2f midPoint(Vector2f v, Vector2f u) {
		return v.add(u).divide(2);
	}

	public static float getAngle(Vector2f v, Vector2f u) {
		Vector2f difference = v.subtract(u);
		return FastMath.atan2(difference.y, difference.x);
	}
}
