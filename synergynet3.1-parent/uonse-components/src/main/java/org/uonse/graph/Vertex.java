package org.uonse.graph;

import java.util.ArrayList;

/**
 * Generic vertex class
 *
 * @param <E> Edge Type
 *
 * @author Brendan Annable
 */
public class Vertex<E extends Edge> {
    // name of the vertex
    private String name = "";
    // list of edges connected to the vertex
    private ArrayList<E> edges = new ArrayList<>();

    public Vertex(String name) {
        this.setName(name);
    }

    /**
     * @param edge edge to add
     */
    public void addEdge(E edge) {
        getEdges().add(edge);
    }

    /**
     * @param edge edge to remove
     *
     * @return true if successful
     */
    public boolean removeEdge(E edge) {
        return getEdges().remove(edge);
    }

    /**
     * @return name of the vertex
     */
    public String getName() {
        return name;
    }

    /**
     * @param name name of the vertex
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return list of edges connected to the vertex
     */
    public ArrayList<E> getEdges() {
        return edges;
    }

    /**
     * @return list of outgoing edges connected to the vertex
     */
    public ArrayList<E> getOutEdges() {
        ArrayList<E> list = new ArrayList<>();
        for (E edge : edges) {
            // add to list if not directed or the source is this vertex
            if (!edge.isDirected() || edge.getSource().equals(this)) {
                list.add(edge);
            }
        }
        return list;
    }

    /**
     * @param edges the edges to set
     */
    public void setEdges(ArrayList<E> edges) {
        this.edges = edges;
    }

    @Override
    public String toString() {
        return getName();
    }
}
