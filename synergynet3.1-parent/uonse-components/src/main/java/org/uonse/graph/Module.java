package org.uonse.graph;

import org.uonse.components.ICircle;

public class Module extends Vertex<Dependency> {

	private ICircle circle;

	public Module(String name) {
		super(name);
	}

	public ICircle getCircle() {
		return circle;
	}

	public void setCircle(ICircle circle) {
		this.circle = circle;
	}
}
