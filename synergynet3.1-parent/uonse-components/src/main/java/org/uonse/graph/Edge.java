package org.uonse.graph;

/**
 * Generic edge class
 *
 * @param <V> Vertex Type
 *
 * @author Brendan Annable
 */
public class Edge<V extends Vertex> {
    /**
     * source vertex
     */
    private V source = null;
    /**
     * target vertex
     */
    private V target = null;
    /**
     * true if edge is directed (one-way)
     */
    private boolean directed = false;
    /**
     * the weight of the edge
     */
    private double weighting = 0;

    public Edge(V source, V target, boolean directed, double weighting) {
        this(source, target);
        this.weighting = weighting;
        this.directed = directed;
    }

    public Edge(V source, V target, double weighting) {
        this(source, target);
        this.weighting = weighting;
    }

    public Edge(V source, V target, boolean directed) {
        this(source, target);
        this.directed = directed;
    }

    public Edge(V source, V target) {
        this.source = source;
        this.source.addEdge(this);
        this.target = target;
        this.target.addEdge(this);
    }

    /**
     * @return the source vertex
     */
    public V getSource() {
        return source;
    }

    /**
     * @param source the source vertex
     */
    public void setSource(V source) {
        this.source.removeEdge(this);
        this.source = source;
        this.source.addEdge(this);
    }

    /**
     * @return the target vertex
     */
    public V getTarget() {
        return target;
    }

    /**
     * @param target the target vertex
     */
    public void setTarget(V target) {
        this.target.removeEdge(this);
        this.target = target;
        this.target.addEdge(this);
    }

    /**
     * @return true if directed
     */
    public boolean isDirected() {
        return directed;
    }

    /**
     * @param directed true if directed
     */
    public void setDirected(boolean directed) {
        this.directed = directed;
    }

    /**
     * @return the weighting of the edge
     */
    public double getWeighting() {
        return weighting;
    }

    /**
     * @param weighting the weighting of the edge
     */
    public void setWeighting(double weighting) {
        this.weighting = weighting;
    }

    @Override
    public String toString()
    {
        return String.format("%s -(%.0f)-> %s", source, getWeighting(), target);
    }
}
