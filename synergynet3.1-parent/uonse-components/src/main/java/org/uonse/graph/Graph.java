package org.uonse.graph;

import java.util.*;

/**
 * Generic graph class
 *
 * @param <V> Vertex Type
 * @param <E> Edge Type
 *
 * @author Brendan Annable
 */
public class Graph<V extends Vertex, E extends Edge> {
    private ArrayList<V> vertices = new ArrayList<>();

    /**
     * add a vertex
     *
     * @param vertex vertex to add
     */
    public void addVertex(V vertex) {
        getVertices().add(vertex);
    }

    /**
     * remove a vertex
     *
     * @param vertex vertex to remove
     */
    public void removeVertex(V vertex) {
        getVertices().remove(vertex);
    }

     /**
     * Calculate the shortest path between two vertices
     *
     * @param source source station
     * @param target target station
     * @return a list of edges that make up the path
     */
    public List<E> shortestPath(V source, V target) {
        // use dijkstras algorithm
        HashMap<V, E> previous = dijkstra(source);
        List<E> path = new ArrayList<>();
        if (!previous.containsKey(target)) {
            // could not be reached
            return null;
        }
        // build path from target backwards
        for (E edge = previous.get(target); edge != null && !edge.getTarget().equals(source); edge = previous.get(edge.getSource())) {
            path.add(edge);
        }
        // reverse the path
        Collections.reverse(path);
        return path;
    }

    public HashMap<V, E> dijkstra(V source) {
        // stores the minimum distance for each vertex
        HashMap<V, Double> minDistances = new HashMap<>();
        // stores the previous most optimal edge needed to be taken for each vertex
        HashMap<V, E> previous = new HashMap<>();
        // the vertex queue of vertices yet to be explored/processed
        PriorityQueue<V> vertexQueue = new PriorityQueue<>(10, new VertexComparator(minDistances));
        // add source to queue
        vertexQueue.add(source);
        // initialise as 0
        minDistances.put(source, 0.0);

        // loop while there is still more vertices to process
        while (!vertexQueue.isEmpty()) {
            // get the first on the queue
            V vertex = vertexQueue.poll();
            // initialise if it hasn't been seen before
            if (!minDistances.containsKey(vertex)) {
                minDistances.put(vertex, Double.MAX_VALUE);
            }
            // get a list of outgoing edges
            ArrayList<E> edges = vertex.getOutEdges();
            // loop edges
            for (E edge : edges) {
                // get the neighbour vertex
                V neighbour = (V) edge.getTarget();
                // initialise if it hasn't been seen before
                if (!minDistances.containsKey(neighbour)) {
                    minDistances.put(neighbour, Double.MAX_VALUE);
                }
                // get edge weighting
                double weight = edge.getWeighting();
                // calculate the total distance to get to this vertex
                double distanceThroughVertex = minDistances.get(vertex) + weight;
                // if it is less than the previously found path, override
                if (distanceThroughVertex < minDistances.get(neighbour)) {
                    // remove from queue
                    vertexQueue.remove(neighbour);
                    // update the distance for the vertex
                    minDistances.put(neighbour, distanceThroughVertex);
                    // update the previous most optimal edge for the vertex
                    previous.put(neighbour, edge);
                    // add back to queue
                    vertexQueue.add(neighbour);
                }
            }
        }

        return previous;
    }

    /**
     * list of vertices
     */
    public ArrayList<V> getVertices() {
        return vertices;
    }

    public void setVertices(ArrayList<V> vertices) {
        this.vertices = vertices;
    }

    /**
     * Vertex comparator, gets given the hashmap of minimum distances to use to compare two vertices
     */
    private class VertexComparator implements Comparator<V> {
        HashMap<V, Double> minDistances;


        public VertexComparator(HashMap<V, Double> minDistances) {
            this.minDistances = minDistances;
        }

        @Override
        public int compare(V vertex1, V vertex2) {
            return Double.compare(minDistances.get(vertex1), minDistances.get(vertex2));
        }
    }
}
