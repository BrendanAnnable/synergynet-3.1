package org.uonse.graph;

import multiplicity3.csys.items.shapes.IColourRectangle;

public class Dependency extends Edge<Module> {
	private IColourRectangle edge;

	public Dependency(Module source, Module target, boolean directed, double weighting) {
		super(source, target, directed, weighting);
	}

	public IColourRectangle getEdge() {
		return edge;
	}

	public void setEdge(IColourRectangle edge) {
		this.edge = edge;
	}
}
