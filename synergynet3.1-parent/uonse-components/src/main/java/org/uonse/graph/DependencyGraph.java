package org.uonse.graph;

public class DependencyGraph extends Graph<Module, Dependency> {

	public Dependency connect(Module source, Module target) {
		return new Dependency(source, target, true, 1);
	}
}
