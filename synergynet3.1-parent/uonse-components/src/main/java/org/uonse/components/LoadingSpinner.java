package org.uonse.components;

import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import com.jme3.system.NanoTimer;
import com.jme3.system.Timer;
import multiplicity3.csys.IUpdateable;
import multiplicity3.csys.annotations.ImplementsContentItem;
import multiplicity3.jme3csys.annotations.RequiresUpdate;
import multiplicity3.jme3csys.geometry.CenteredQuad;
import multiplicity3.jme3csys.picking.ItemMap;
import org.uonse.app.Component;

import java.util.UUID;

@RequiresUpdate
@ImplementsContentItem(target = ILoadingSpinner.class)
public class LoadingSpinner extends Component implements ILoadingSpinner, IUpdateable {

	private float radius = 1.0F;
	private float ringWidth = 0.2F;
	private float speed = 1.0F;

	private Geometry geometry = null;
	private Timer timer = null;

	public LoadingSpinner(String name, UUID uuid) {
		super(name, uuid);
	}

	@Override
	public void init() {
		geometry = createSpinner();
		timer = new NanoTimer();
	}

	private Geometry createSpinner() {
		// Create a quad mesh
		CenteredQuad quad = new CenteredQuad(1, 1);

		// Create a new geometry, using the quad as the mesh
		Geometry geometry = new Geometry("Loading Spinner", quad);

		// Load material definition from J3MD File
		Material material = new Material(assetManager, "org/uonse/components/LoadingSpinner/LoadingSpinner.j3md");

		// Enable transparency blend mode
		material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);

		// Configure shader uniforms
		material.setFloat("Radius", getRadius());
		material.setFloat("RingWidth", getRingWidth());
		material.setFloat("Period", 1.0F / getSpeed());

		geometry.setMaterial(material);
		setRelativeScale(70);

		// Register the item with Multiplicity
		ItemMap.register(geometry, this);

		// Add spatial to the node
		attachChild(geometry);

		return geometry;
	}

	@Override
	public Spatial getManipulableSpatial() {
		return geometry;
	}

	public void update(float timePerFrameSeconds) {
		geometry.getMaterial().setFloat("Time", timer.getTimeInSeconds());
	}

	public float getRadius() {
		return radius;
	}

	public void setRadius(float radius) {
		this.radius = radius;
		geometry.getMaterial().setFloat("Radius", getRadius());
	}

	public float getSpeed() {
		return speed;
	}

	public void setSpeed(float speed) {
		this.speed = speed;
		geometry.getMaterial().setFloat("Period", 1.0F / getSpeed());
	}

	public float getRingWidth() {
		return ringWidth;
	}

	public void setRingWidth(float ringWidth) {
		this.ringWidth = ringWidth;
		geometry.getMaterial().setFloat("RingWidth", ringWidth);
	}

	public void setSize(float size) {
		setRelativeScale(size);
	}

	public float getSize() {
		return getRelativeScale();
	}

}
