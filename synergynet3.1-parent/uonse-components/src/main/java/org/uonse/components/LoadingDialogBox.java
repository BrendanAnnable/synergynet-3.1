package org.uonse.components;

import com.jme3.font.BitmapText;
import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.math.Vector2f;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import multiplicity3.csys.annotations.ImplementsContentItem;
import multiplicity3.csys.items.item.IItem;
import multiplicity3.csys.items.mutablelabel.IMutableLabel;
import multiplicity3.jme3csys.geometry.CenteredQuad;
import multiplicity3.jme3csys.picking.ItemMap;
import org.uonse.app.Component;

import java.util.UUID;

@ImplementsContentItem(target = ILoadingDialogBox.class)
public class LoadingDialogBox extends Component implements ILoadingDialogBox {

	private float width = 560F;
	private float height = 220F;
	private Geometry geometry;

	public LoadingDialogBox(String name, UUID uuid) {
		super(name, uuid);
	}

	@Override
	public void init() {

		Geometry shadow = createDropShadow(width, height, 4);
		attachChild(shadow);

		geometry = createBox(width, height);
		attachChild(geometry);

		IItem spinner = createSpinner();
		spinner.setRelativeLocation(new Vector2f(-180, 0));
		addItem(spinner);

		//IItem text = createText();
	}

	public Geometry createDropShadow(float width, float height, float offset) {
		CenteredQuad quad = new CenteredQuad(width, height);
		Geometry geometry = new Geometry(this.getClass().getName(), quad);
		Material material = new Material(assetManager, "org/uonse/components/Shadow/Shadow.j3md");
		material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);
		geometry.setMaterial(material);
		geometry.setLocalTranslation(offset, -offset, 0);

		ItemMap.register(geometry, this);

		return geometry;
	}

	public Geometry createBox(float width, float height) {
		CenteredQuad quad = new CenteredQuad(width, height);
		Geometry geometry = new Geometry(this.getClass().getName(), quad);
		Material material = new Material(assetManager, "org/uonse/components/LoadingDialogBox/LoadingDialogBox.j3md");
		geometry.setMaterial(material);

		ItemMap.register(geometry, this);

		return geometry;
	}

	private IItem createSpinner() {
		ILoadingSpinner spinner = contentFactory.create(ILoadingSpinner.class);
		spinner.setRelativeLocation(new Vector2f(-180, 0));
		return spinner;
	}

	private IItem createText() {
		IMutableLabel label = contentFactory.create(IMutableLabel.class);
		label.setText("Loading...");
		BitmapText txt = (BitmapText) label.getManipulableSpatial();
		txt.setColor(ColorRGBA.Black);
		return label;
	}

	public Spatial getManipulableSpatial() {
		return geometry;
	}
}
