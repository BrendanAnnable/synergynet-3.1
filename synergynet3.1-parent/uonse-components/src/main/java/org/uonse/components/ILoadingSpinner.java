package org.uonse.components;
import org.uonse.app.IComponent;

public interface ILoadingSpinner extends IComponent {

	float getRadius();
	void setRadius(float radius);
	float getSpeed();
	void setSpeed(float speed);
	float getSize();
	void setSize(float size);
	float getRingWidth();
	void setRingWidth(float size);

}
