package org.uonse.components;

import com.jme3.material.Material;
import com.jme3.material.RenderState;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.Spatial;
import multiplicity3.csys.annotations.ImplementsContentItem;
import multiplicity3.jme3csys.picking.ItemMap;
import org.uonse.app.Component;
import org.uonse.components.mesh.CircleMesh;

import java.util.UUID;

@ImplementsContentItem(target = ICircle.class)
public class Circle extends Component implements ICircle {

	private Geometry geometry = null;
	private ColorRGBA color = ColorRGBA.Blue;

	public Circle(String name, UUID uuid) {
		super(name, uuid);
	}

	@Override
	public void init() {
		Geometry circle = createCircle();
		attachChild(circle);

		geometry = circle;
	}

	private Geometry createCircle() {
		// Create a quad mesh
		CircleMesh quad = new CircleMesh(1, 64);

		// Create a new geometry, using the quad as the mesh
		Geometry geometry = new Geometry("Loading Spinner", quad);

		// Load material definition from J3MD File
		Material material = new Material(assetManager, "org/uonse/components/Circle/Circle.j3md");

		material.setColor("Color", color);

		// Enable transparency blend mode
		material.getAdditionalRenderState().setBlendMode(RenderState.BlendMode.Alpha);

		geometry.setMaterial(material);
		setRelativeScale(70);

		// Register the item with Multiplicity
		ItemMap.register(geometry, this);

		// Add spatial to the node
		attachChild(geometry);

		return geometry;

	}

	/**
	 * Gets the manipulable spatial.
	 *
	 * @return the manipulable spatial
	 */
	@Override
	public Spatial getManipulableSpatial() {
		return geometry;
	}

	@Override
	public float getSize(float size) {
		return getRelativeScale();
	}

	@Override
	public void setSize(float size) {
		setRelativeScale(size);
	}

	@Override
	public ColorRGBA getColor() {
		return color;
	}

	@Override
	public void setColor(ColorRGBA color) {
		this.color = color;
		this.geometry.getMaterial().setColor("Color", color);
	}
}
