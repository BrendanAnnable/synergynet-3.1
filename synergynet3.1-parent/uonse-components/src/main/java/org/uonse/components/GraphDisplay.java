package org.uonse.components;

import com.jme3.math.ColorRGBA;
import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.scene.Spatial;
import multiplicity3.csys.IUpdateable;
import multiplicity3.csys.annotations.ImplementsContentItem;
import multiplicity3.csys.behaviours.RotateTranslateScaleBehaviour;
import multiplicity3.csys.items.shapes.IColourRectangle;
import multiplicity3.jme3csys.annotations.RequiresUpdate;
import org.uonse.app.Component;
import org.uonse.graph.Dependency;
import org.uonse.graph.DependencyGraph;
import org.uonse.graph.Module;
import org.uonse.util.FasterMath;
import org.uonse.util.ColorUtils;

import java.util.ArrayList;
import java.util.UUID;

@RequiresUpdate
@ImplementsContentItem(target = IGraphDisplay.class)
public class GraphDisplay extends Component implements IGraphDisplay, IUpdateable {

	private DependencyGraph graph = null;

	public GraphDisplay(String name, UUID uuid) {
		super(name, uuid);
	}

	public void init() {
		graph = createGraph();
		displayGraph(graph);
	}

	private DependencyGraph createGraph() {
		DependencyGraph graph = new DependencyGraph();
		Module multiplicityConfig = new Module("Multiplicity Config");
		Module multiplicityInput = new Module("Multiplicity Input");
		Module multiplicityAppSystem = new Module("Multiplicity App System");
		Module multiplicityContentSystem = new Module("Multiplicity Content System");
		Module multiplicityJMEContentSystem = new Module("Multiplicity JME Content System");
		Module synergynetConfig = new Module("Synergynet Config");
		Module synergynetCluster = new Module("Synergynet Cluster");
		Module synergynetAppSystemCore = new Module("Synergynet App System Core");

		graph.addVertex(multiplicityConfig);
		graph.connect(multiplicityConfig, multiplicityInput);

		graph.addVertex(multiplicityInput);

		graph.addVertex(multiplicityAppSystem);
		graph.connect(multiplicityAppSystem, multiplicityInput);
		graph.connect(multiplicityAppSystem, multiplicityConfig);
		graph.connect(multiplicityAppSystem, multiplicityJMEContentSystem);
		graph.connect(multiplicityAppSystem, multiplicityContentSystem);

		graph.addVertex(multiplicityContentSystem);
		graph.connect(multiplicityContentSystem, multiplicityInput);

		graph.addVertex(multiplicityJMEContentSystem);
		graph.connect(multiplicityJMEContentSystem, multiplicityContentSystem);
		graph.connect(multiplicityJMEContentSystem, multiplicityInput);

		graph.addVertex(synergynetConfig);
		graph.connect(synergynetConfig, multiplicityConfig);

		graph.addVertex(synergynetCluster);
		graph.connect(synergynetCluster, synergynetConfig);
		graph.connect(synergynetCluster, multiplicityConfig);

		graph.addVertex(synergynetAppSystemCore);
		graph.connect(synergynetAppSystemCore, synergynetCluster);
		graph.connect(synergynetAppSystemCore, synergynetConfig);
		graph.connect(synergynetAppSystemCore, multiplicityConfig);
		graph.connect(synergynetAppSystemCore, multiplicityInput);
		graph.connect(synergynetAppSystemCore, multiplicityAppSystem);
		graph.connect(synergynetAppSystemCore, multiplicityJMEContentSystem);

		return graph;
	}

	private void displayGraph(DependencyGraph graph) {

		float circleSize = 100;
		float radius = 300;

		ArrayList<Module> modules = graph.getVertices();
		for (int i = 0; i < modules.size(); i++) {
			Module module = modules.get(i);

			ICircle circle = contentFactory.create(ICircle.class);
			circle.setSize(circleSize);
			behaviourMaker.addBehaviour(circle, RotateTranslateScaleBehaviour.class);
			circle.setRelativeLocation(FasterMath.fromPolar(radius, i * FastMath.TWO_PI / modules.size()));
			circle.setColor(ColorUtils.HSV_to_RGBA(i / (float) modules.size(), 1, 1));

			module.setCircle(circle);
		}

		for (Module module : modules) {
			for (Dependency dependency : module.getOutEdges()) {
				IColourRectangle edge = contentFactory.create(IColourRectangle.class);
				edge.setSolidBackgroundColour(ColorRGBA.Black);
				addItem(edge);

				dependency.setEdge(edge);
			}
		}

		updateEdgePositions(graph);

		for (Module module : modules) {
			addItem(module.getCircle());
		}

	}

	private void updateEdgePositions(DependencyGraph graph) {
		float edgeSize = 7;

		for (Module module : graph.getVertices()) {
			for (Dependency dependency : module.getOutEdges()) {

				IColourRectangle edge = dependency.getEdge();
				Vector2f sourceLocation = dependency.getSource().getCircle().getRelativeLocation();
				Vector2f targetLocation = dependency.getTarget().getCircle().getRelativeLocation();
				edge.setSize(sourceLocation.distance(targetLocation), edgeSize);
				edge.setRelativeLocation(FasterMath.midPoint(sourceLocation, targetLocation));
				edge.setRelativeRotation(FasterMath.getAngle(sourceLocation, targetLocation));
			}
		}
	}

	@Override
	public void update(float timePerFrameSeconds) {
		updateEdgePositions(graph);
	}

	@Override
	public Spatial getManipulableSpatial() {
		return null;
	}
}
