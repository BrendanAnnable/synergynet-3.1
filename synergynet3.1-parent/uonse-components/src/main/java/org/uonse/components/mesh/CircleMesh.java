package org.uonse.components.mesh;

import com.jme3.math.FastMath;
import com.jme3.math.Vector2f;
import com.jme3.math.Vector3f;
import com.jme3.scene.Mesh;
import com.jme3.scene.VertexBuffer;
import com.jme3.util.BufferUtils;

public class CircleMesh extends Mesh {

	private final int numSegments;
	private final float radius;

	public CircleMesh(float radius, int numSegments) {
		this.radius = radius;
		this.numSegments = numSegments;
		initBuffers();
	}

	private void initBuffers() {
		int numVertices = numSegments + 1;
		Vector3f[] vertices = new Vector3f[numVertices];
		Vector3f[] normals = new Vector3f[numVertices];
		Vector2f[] uvs = new Vector2f[numVertices];
		int[] indices = new int[numSegments * 3];

		vertices[0] = Vector3f.ZERO;
		normals[0] = Vector3f.UNIT_Z;
		uvs[0] = new Vector2f(0.5F, 0.5F);

		for (int i = 0; i < numSegments; i++) {
			float angle = i * FastMath.TWO_PI / numSegments;
			Vector3f vertex = new Vector3f(radius * FastMath.cos(angle) / 2.0F, radius * FastMath.sin(angle) / 2.0F, 0);
			vertices[i + 1] = vertex;
			uvs[i + 1] = new Vector2f(vertex.x / radius + 0.5F, vertex.y / radius + 0.5F);
			normals[i + 1] = Vector3f.UNIT_Z;

			int offset = i * 3;
			indices[offset    ] = i + 1;
			indices[offset + 1] = i < numSegments - 1 ? i + 2 : 1;
			indices[offset + 2] = 0;
		}

		setBuffer(VertexBuffer.Type.Position, 3, BufferUtils.createFloatBuffer(vertices));
		setBuffer(VertexBuffer.Type.Normal, 3, BufferUtils.createFloatBuffer(normals));
		setBuffer(VertexBuffer.Type.TexCoord, 2, BufferUtils.createFloatBuffer(uvs));
		setBuffer(VertexBuffer.Type.Index, 3, BufferUtils.createIntBuffer(indices));

		updateBound();
	}
}
