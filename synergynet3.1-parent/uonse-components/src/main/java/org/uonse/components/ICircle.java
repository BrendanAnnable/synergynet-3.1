package org.uonse.components;

import com.jme3.math.ColorRGBA;
import org.uonse.app.IComponent;

public interface ICircle extends IComponent {
	void setSize(float size);
	float getSize(float size);
	void setColor(ColorRGBA color);
	ColorRGBA getColor();
}
